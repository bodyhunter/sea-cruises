var link = document.querySelector('.menu-nav');
var popup = document.querySelector('.navigation');
var close = popup.querySelector('.menu-close');

link.addEventListener('click', function (evt) {
    popup.classList.add('modal-show');
});

close.addEventListener('click', function (evt) {
    evt.preventDefault();
    popup.classList.remove('modal-show');
});

var linkmodal = document.querySelector('.smail-modal');
var popupblock = document.querySelector('.modal-block');
var closemodal = popupblock.querySelector('.button-close');

linkmodal.addEventListener('click', function (evt) {
    popupblock.classList.add('modal-show');
});

closemodal.addEventListener('click', function (evt) {
    evt.preventDefault();
    popupblock.classList.remove('modal-show');
});